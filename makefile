COMP=g++
MAIN=src/main.cpp
BIN=bin/
NAME_DEBUG=test
NAME_RELEASE=SimpleDB

debug :
	$(COMP) $(MAIN) -o $(BIN)$(NAME_DEBUG) -Ofast -Wall

release :
	$(COMP) $(MAIN) -o $(BIN)$(NAME_RELEASE) -Ofast
