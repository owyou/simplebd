bool shortEnough(char* str, int len) {
	return (signed)strlen(str) < (signed)len;
}

void flushInputBuffer() {
	while (getchar() != '\n') {
	}
}

void inputString(char* str, int len) {
	int result = 0;

	result = scanf("%s", str);
	while (!(result && shortEnough(str, len))) {
		printf("%s\n", "Input error. Please, try again");
		result = scanf("%s", str);
		flushInputBuffer();
	}
}

void inputInteger(int* num) {
	int result = 0;

	result = scanf("%d", num);
	while (!result) {
		printf("%s\n", "Input error. Please, try again");
		flushInputBuffer();
		result = scanf("%d", num);
	}
	flushInputBuffer();
}
