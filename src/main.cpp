/* baza dannih bibliotek. polya: nomer biblioteki, adres, telefon */
/* vvod vivod dobavlenie udalenie */

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stddef.h>
#include "Check.cpp"
#include "CData.cpp"
#include "CNode.cpp"
#include "CList.cpp"
#include "CMenu.cpp"
#include "CFile.cpp"
#include "IO.cpp"

int main() {	
	CMenu* menu = (CMenu*)malloc(sizeof(CMenu));	
	CList* list = (CList*)malloc(sizeof(CList));
	int menu_size = 7;

	list->init = CList_init;
	list->init(list);
	menu->init = CMenu_init;
	menu->init(menu);

	menu->createList(menu, menu_size);
	
	menu->addItem(menu, (char*)"Add", add);
	menu->addItem(menu, (char*)"Change", change);
	menu->addItem(menu, (char*)"Remove", del);
	menu->addItem(menu, (char*)"Write to file", fileWrite);
	menu->addItem(menu, (char*)"Read from file", fileRead);
	menu->addItem(menu, (char*)"Print all", printAll);
	menu->addItem(menu, (char*)"Exit", ext);

	while (menuUpdate(list, menu, menu_size) != menu_size) {}

	list->clear(list);
	menu->clear(menu);

	free(list);
	free(menu);

	return 0;
}

