#include "CMenu.h"

void CMenu_destroyList(CMenu* owner) {
	free(owner->list);
}

int CMenu_listLength(CMenu* owner) {
//	return (sizeof(owner->list) / sizeof(CStrAndCom));
	return owner->length;
}

void CMenu_evokeItem(CMenu* owner, int num, void* data) {
	//printf("%s%d%s%s\n", "List item ", num, " is ", owner->list[num].str);
	owner->list[num].com(data);
}

void CMenu_createList(CMenu* owner, int size) {
	owner->list = (CStrAndCom*)malloc(sizeof(CStrAndCom)*size);
}

void CMenu_addItem(CMenu *owner, char* item, void (*func)(void* data)) {
	int len = owner->length;	
	owner->list[len].str = item;
	owner->list[len].com = func;	
	owner->length++;
}

void CMenu_printMenu(CMenu* owner) {
	for (int i=0; i<owner->length; i++) {
		printf("%d%s%s\n", (i+1), ") ", owner->list[i].str);
	}
}

void CMenu_init(CMenu* owner) {
	owner->listLength = CMenu_listLength;
	owner->createList = CMenu_createList;
	owner->addItem = CMenu_addItem;
	owner->evokeItem = CMenu_evokeItem;
	owner->printMenu = CMenu_printMenu;
	owner->destroyList = CMenu_destroyList;
	owner->length = 0;
}

