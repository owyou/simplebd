#include "CList.h"

void CList_addItem(CList* owner, int num, char* adr, char* ph) {	
	CNode* item = (CNode*)malloc(sizeof(CNode));
	item->init = CNode_init;
	item->init(item);

	item->createData(item, num, adr, ph);
	
	item->setNext(item, owner->first);
	owner->first = item;
	owner->length++;
}

CNode* CList_getItem(CList* owner, int num) {
	int count = 0;
	CNode* node = owner->first;
	while (count < num) {
		node = node->next;
		count++;
	}	
	return node;
}

void CList_delItem(CList* owner, int num) {
	if (num == 0) {
		CNode* node = owner->first;
		owner->first = node->next;
		free(node);
	} else {
		CNode* node = owner->getItem(owner, num);
		owner->getItem(owner, num-1)->next = node->next;
		free(node);
	}
	owner->length--;
}

void CList_changeItem(CList* owner, int n, int num, char* adr, char* ph) {
	CNode* node = owner->getItem(owner, n);
	CData* data = node->getData(node);

	data->setNum(data, num);
	data->setAdr(data, adr);
	data->setPh(data, ph);
}


void CList_clear(CList* owner) {
	CNode* node = owner->first;
	CNode* prev = NULL;
	CData* data = NULL;

	while (node) {
		data = node->getData(node);
		data->clear(data);
		free(data);
		prev = node;
		node = node->next;
		free(prev);
	}
	free(node);
}

void CList_init(CList* owner) {
	owner->addItem = CList_addItem;
	owner->getItem = CList_getItem;
	owner->delItem = CList_delItem;
	owner->changeItem = CList_changeItem;
	owner->clear = CList_clear;
	owner->first = NULL;
	owner->length = 0;
}
