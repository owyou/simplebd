#include "CNode.h"

void CNode_setData(CNode* owner, CData* data) {
	owner->data = data;
}

void CNode_setNext(CNode* owner, CNode* next) {
	owner->next = next;
}

void CNode_createData(CNode* owner, int num, char* adr, char* ph) {
	CData* data = (CData*)malloc(sizeof(CData));
	
	data->init = CData_init;
	data->init(data);
	data->setNum(data, num);
	data->setAdr(data, adr);
	data->setPh(data, ph);
	owner->data = data;
}

void CNode_destroyData(CNode* owner) {
	free(owner->data);
}

CData* CNode_getData(CNode* owner) {
	return owner->data;
}

CNode* CNode_getNext(CNode* owner) {
	return owner->next;
}

void CNode_init(CNode* owner) {
	owner->setData = CNode_setData;
	owner->setNext = CNode_setNext;
	owner->getData = CNode_getData;
	owner->getNext = CNode_getNext;
	owner->createData = CNode_createData;
	owner->destroyData = CNode_destroyData;
}


