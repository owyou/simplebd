#include "CData.h"

void CData_del(CData* owner) {
}

int CData_getNum(CData* owner) {
	return owner->num;
}

char* CData_getAdr(CData* owner) {
	return owner->adr;
}

char* CData_getPh(CData* owner) {
	return owner->ph;
}

void CData_setNum(CData* owner, int num) {
	owner->num = num;
}

void CData_setAdr(CData* owner, char* adr) {
	owner->adr = adr;
}

void CData_setPh(CData* owner, char* ph) {
	owner->ph = ph;
}

void CData_clear(CData* owner) {
	free(owner->adr);
	free(owner->ph);
}

void CData_init(CData* owner) {
	owner->num = 0;
	owner->adr = NULL;
	owner->ph = NULL;

	owner->clear = CData_clear;
	owner->del = CData_del;
	owner->getNum = CData_getNum;
	owner->getAdr = CData_getAdr;
	owner->getPh = CData_getPh;
	owner->setNum = CData_setNum;
	owner->setAdr = CData_setAdr;
	owner->setPh = CData_setPh;
}
