struct CData {
	int num;
	char* adr;
	char* ph;

	void (*init)(CData* owner);
	void (*del)(CData* owner);
	int (*getNum)(CData* owner);
	char* (*getAdr)(CData* owner);
	char* (*getPh)(CData* owner);
	void (*setNum)(CData* owner, int num);
	void (*setAdr)(CData* owner, char* adr);
	void (*setPh)(CData* owner, char* ph);
	void (*clear)(CData* owner);
};
