struct CStrAndCom {
	char* str;
	void (*com)(void* data);
};

struct CMenu {
	CStrAndCom* list;
	int length;

	int (*listLength)(CMenu* owner);
	void (*createList)(CMenu* owner, int size);
	void (*addItem)(CMenu* owner, char* item, void (*func)(void* data));
	void (*evokeItem)(CMenu* owner, int num, void* data);
	void (*printMenu)(CMenu* owner); 
	void (*destroyList)(CMenu* owner);
	void (*init)(CMenu* owner);
};


