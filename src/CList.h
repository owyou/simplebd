struct CList {
	CNode* first;
	int length;

	void (*addItem)(CList* owner, int num, char* adr, char* ph);
	CNode* (*getItem)(CList* owner, int num);
	void (*delItem)(CList* owner, int num);
	void (*changeItem)(CList* owner, int n, int num, char* adr, char* ph);
	void (*init)(CList* owner);
	void (*clear)(CList* owner);
};


