#include "CFile.h"

int CFile_open(CFile* owner, char* filename, char* mode) {
	owner->file = fopen(filename, mode);
	if (owner->file == NULL) {
		return 0;
	} else {
		return 1;
	}
}

int CFile_close(CFile* owner) {
	if (owner->file == NULL) {
		return 0;
	} else {
		if (fclose(owner->file)) {
			return 1;
		} else {
			return 0;
		}
	}
}

int CFile_writeInt(CFile* owner, int num) {
	if (owner->file == NULL) {
		return 0;
	} else {
		if (fwrite(&num, sizeof(int), 1, owner->file)) {
			return 1;
		} else {
			return 0;
		}
	}
}

int CFile_writeString(CFile* owner, char* str) {
	if (owner->file == NULL) {
		return 0;
	} else {
		char b = strlen(str);
		if (fwrite(&b, sizeof(char), 1, owner->file)) {
			for (int i=0; i<b; i++) {
				fwrite(&str[i], sizeof(char), 1, owner->file);
			}
			return 1;
		} else {
			return 0;
		}
	}
}

int CFile_readInt(CFile* owner) {
	if (owner->file != NULL) {
		int n;
		if (fread(&n, sizeof(int), 1, owner->file)) {
			return n;
		} else {
			return 0;
		}
	} else {
		return 0;
	}
}

char* CFile_readString(CFile* owner) {
		char b = 0;
		fread(&b, sizeof(char), 1, owner->file);
		char* s = (char*)malloc(sizeof(char)*255);
		for (int i=0; i<b; i++) {
			fread(&s[i], sizeof(char), 1, owner->file);
		}
		return s;
}

void CFile_init(CFile* owner) {
	owner->open = CFile_open;
	owner->close = CFile_close;
	owner->writeInt = CFile_writeInt;
	owner->writeString = CFile_writeString;
	owner->readInt = CFile_readInt;
	owner->readString = CFile_readString;
	owner->file = NULL;
}
