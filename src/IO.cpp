void flush() {
	printf("\033[2J\033[1;1H");
}

void wait() {
	while (getchar() != '\n') {
	}
}

void fileRead(void* data) {
	CList* list = (CList*)data;
	CFile* f = (CFile*)malloc(sizeof(CFile));
	f->init = CFile_init;
	f->init(f);

	if (f->open(f, (char*)"./f", (char*)"r")) {
		int num = 0;
		int len = 0;
		char* adr = NULL;
		char* ph = NULL;
		if (((len = f->readInt(f)))) {
			for (int i=0; i<len; i++) {
				if (((num = f->readInt(f)))) {
					if (((adr = f->readString(f)))) {
						if (((ph = f->readString(f)))) {
							list->addItem(list, num, adr, ph);
							printf("%s%d\n", "Read item #", (i+1));
						} else {
							printf("%s%d\n", "Error reading Phone of item #",
i);
						}
					} else {
						printf("%s%d\n", "Error reading Adress of item #", i);
					}
				} else {
					printf("%s%d\n", "Error reading Number of item #", i);
				}
			}
			if (f->close(f)) {
				printf("%s\n", "Read successfully");
			} else {
//				printf("%s\n", "Error closing the file");
			}
		} else {
			printf("%s\n", "Unknown file type");
		}
	} else {
		printf("%s\n", "Error opening the file");
	}
	wait();
	free(f);
}

void fileWrite(void* data) {
	CList* list = (CList*)data;
	CNode* node = list->first;
	CData* info = NULL;
	CFile* f = (CFile*)malloc(sizeof(CFile));
	f->init = CFile_init;
	f->init(f);

	if (list->length == 0) {
		printf("%s\n", "The list is empty!");
		wait();
	} else {

		int i = 1;

		if (f->open(f, (char*)"./f", (char*)"w")) {
			if (f->writeInt(f, list->length)) {
				while (node) {
					info = node->getData(node);
					
					if (f->writeInt(f, info->getNum(info))) {
						if (f->writeString(f, info->getAdr(info))) {
							if (f->writeString(f, info->getPh(info))) {
								printf("%s%d\n", "Wrote item #", i);
									i++;
							} else {
							printf("%s%d\n", "Error writing Phone of item #", i);
							}
						} else {
							printf("%s%d\n", "Error writing Adress of item #", i);
						}
					} else {
						printf("%s%d\n", "Error writing Number of item #", i);
					}
		
					node = node->next;
				}
				if (f->close(f)) {
					printf("%s\n", "Wrote successfully");
				}
			}
		}
		wait();
		free(f);
		
	}
}

void change(void* data) {
	CList* list = (CList*)data;
	int num;

	printf("%s\n", "Item to change:");
	inputInteger(&num);
	
	if (list->length == 0) {
		printf("%s\n", "The list is empty!");
	} else {
		while ((num > list->length) || (num <= 0)) {
			printf("%s\n", "There's no such item in the list. Please, try again");
			inputInteger(&num);
		}

		CNode* node = list->getItem(list, num-1);
		CData* data = node->getData(node);
		int num;
		int str_len = 255;
		char* adr = (char*)malloc(sizeof(char)*str_len);
		char* ph = (char*)malloc(sizeof(char)*str_len);


		printf("%s\n", "Number:");
		inputInteger(&num);
		printf("%s\n", "Address:");
		inputString(adr, str_len);
		printf("%s\n", "Phone:");
		inputString(ph, str_len);

		data->setNum(data, num);
		data->setAdr(data, adr);
		data->setPh(data, ph);	
	}
}

void printAll(void* data) {
	CList* list = (CList*)data;
	CNode* node = list->first;
	CData* info;
	int num;
	char* adr;
	char* ph;
	while (node) {
		info = node->getData(node);
		num = info->getNum(info);
		adr = info->getAdr(info);
		ph = info->getPh(info);
		printf("----------\nNumber: %d\nAddress: %s\nPhone: %s\n", num, adr, ph);
		node = node->next;
	}
	wait();
}


void add(void* data) {
	CList* list = (CList*)data;
	int num = 0;
	int str_len = 255;
	char* adr = (char*)malloc(sizeof(char)*str_len);
	char* ph = (char*)malloc(sizeof(char)*str_len);
	
	printf("%s\n", "Adding an item");
	printf("%s\n", "Number:");
	inputInteger(&num);
	printf("%s\n", "Address:");
	inputString(adr, str_len);
	printf("%s\n", "Phone:");
	inputString(ph, str_len);
	
	list->addItem(list, num, adr, ph);
}

void del(void* data) {
	CList* list = (CList*)data;
	int num;
	
	if (list->length == 0) {
		printf("%s\n", "The list is empty!");
		while (getchar() != '\n') {}
	} else {
		printf("%s\n", "Item to delete:");
		inputInteger(&num);
		while ((num > list->length) || (num <= 0)) {
			printf("%s\n", "There's no such item in the list! Please, try again");
			inputInteger(&num);
		}

		list->delItem(list, num-1);
	}
}

int menuUpdate(CList* list, CMenu* menu, int menu_size) {
	int choice = 0;
	flush();
	menu->printMenu(menu);
	inputInteger(&choice);
	while ((choice > menu_size) || (choice <= 0)) {
		printf("%s\n", "There's no such option! Please, try again");
		inputInteger(&choice);
	}
	menu->evokeItem(menu, choice-1, list);
	return choice;
}

void ext(void* data) {
}


