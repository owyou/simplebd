struct CNode {
	CData* data;
	CNode* next;

	void (*setData)(CNode* owner, CData* data);
	void (*setNext)(CNode* owner, CNode* next);
	void (*createData)(CNode* owner, int num, char* adr, char* ph);
	void (*destroyData)(CNode* owner);
	void (*init)(CNode* owner);
	CData* (*getData)(CNode* owner);
	CNode* (*getNext)(CNode* owner);
};
