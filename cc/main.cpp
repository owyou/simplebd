#include <stdio.h>
#include <stdlib.h>
#include "CFile.cpp"

int main() {
	CFile* file = (CFile*)malloc(sizeof(CFile));

	file->init = CFile_init;
	file->init(file);

	if (file->open(file, "./f", "w")) {
		if (file->writeInt(file, 420)) {
			if (file->writeString(file, "asdfghjkl")) {
				file->writeString(file, "zxcvbnm");
				file->writeInt(file, 999);
				file->writeString(file, "qqqq");
				file->writeString(file, "zzzz");
				if (file->close(file)) {
					printf("%s\n", "Wrote flawlessly");
				} else {
					printf("%s\n", "File close w error");
				}
			} else {
				printf("%s\n", "File write str error");
			}
		} else {
			printf("%s\n", "File write int error");
		}
	} else {
		printf("%s\n", "File open w error");
	}

	if (file->open(file, "./f", "r")) {
		int n1 = file->readInt(file);
		char* s11 = file->readString(file);
		char* s12 = file->readString(file);
		int n2 = file->readInt(file);
		char* s21 = file->readString(file);
		char* s22 = file->readString(file);
		if (file->close(file)) {
			printf("%s%d%s%x\n", "Read: ", n1, "; addr: ", &n1);
			printf("%s%s%s%x\n", "Read: ", s11, "; addr: ", &s11);
			printf("%s%s%s%x\n", "Read: ", s12, "; addr: ", &s12);
			printf("%s%d%s%x\n", "Read: ", n2, "; addr: ", &n2);
			printf("%s%s%s%x\n", "Read: ", s21, "; addr: ", &s21);
			printf("%s%s%s%x\n", "Read: ", s22, "; addr: ", &s22);

		} else {
			printf("%s\n", "File close r error");
		}
	} else {
		printf("%s\n", "File open r error");
	}


	free(file);
	return 0;
}
