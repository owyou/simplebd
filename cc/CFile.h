struct CFile {
	char* filename;
	FILE* file;
	
	int (*open)(CFile* owner, char* filename, char* mode);
	int (*close)(CFile* owner);
	int (*writeInt)(CFile* owner, int num);
	int (*writeString)(CFile* owner, char* str);
	int (*readInt)(CFile* owner);
	char* (*readString)(CFile* owner);
	void (*init)(CFile* owner);
};

